package be.technobel.arlon2021mvc.repositories;

import be.technobel.arlon2021mvc.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findOneByUsernameAndPassword(String username, String password);
}
