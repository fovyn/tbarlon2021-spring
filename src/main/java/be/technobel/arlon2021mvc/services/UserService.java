package be.technobel.arlon2021mvc.services;

import be.technobel.arlon2021mvc.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAll();
    Optional<User> findOneById(Long id);
    User updateOne(Long id, User user);
    User findOneByLogin(String username, String password);
    void deleteOne(long id);
    User insert(User user);
}
