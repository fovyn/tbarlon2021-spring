package be.technobel.arlon2021mvc.controllers;

import be.technobel.arlon2021mvc.entities.User;
import be.technobel.arlon2021mvc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = {"/home"})
public class HomeController {
    private final UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = {"", "/", "/index"})
    public String indexAction(Model view) {
        view.addAttribute("nom", "Flavian");
        view.addAttribute("users", this.userService.findAll());
        view.addAttribute("Flavian", this.userService.findOneByLogin("Flavian", "Test1234="));

        return "home/index";
    }

    @GetMapping(path= {"/list", "/all", "/any"})
    public String listAction() {
        return "home/list";
    }

    @GetMapping(path = {"/edit/{user:[0-9]+}"})
    public String editAction(Model view, @PathVariable("user") User user) {

        view.addAttribute("nom", "Flavian");
        view.addAttribute("users", this.userService.findAll());
        view.addAttribute("Flavian", this.userService.findOneByLogin("Flavian", "Test1234="));
        view.addAttribute("sUser", user);

        return "home/index";
    }
}
